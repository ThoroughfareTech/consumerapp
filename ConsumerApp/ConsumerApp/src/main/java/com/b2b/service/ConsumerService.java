package com.b2b.service;

import com.b2b.utils.HttpUtils;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by vkumar on 1/7/2017.
 */
@Service
public class ConsumerService {

    @Value("${store.consumer.service.url}")
    private String storeconsumerUrl;

    public String storeConsumerInfo(final String consumerJson) {
        String message = "";
        final boolean status = new HttpUtils().connect(storeconsumerUrl, consumerJson, ContentType.APPLICATION_JSON);
        if (!status) {
            message = "Cannot proceed to store consumer information!!!";
            return message;
        }
        return "Successfully stored your information!!!";
    }

}
