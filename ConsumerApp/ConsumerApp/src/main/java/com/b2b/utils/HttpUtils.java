package com.b2b.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by vkumar on 1/7/2017.
 */
public class HttpUtils {

    private final static Logger logger = LoggerFactory.getLogger(HttpUtils.class);
    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    public boolean connect(final String httpUrl, final String content, final ContentType contentType) {

        try {
            final HttpPost httpPost = createRequestPostMethod(httpUrl, content, contentType);
            final HttpUriRequest httpUriRequest = httpPost;
            final HttpResponse httpResponse = httpClient.execute(httpUriRequest);
            return httpResponse.getStatusLine().getStatusCode() == 200;
        } catch (final ClientProtocolException clientProtocolException) {
            logger.error("An exception occurred while making a call to storeCustomer service", clientProtocolException);
        } catch (final IOException ioException) {
            logger.error("An exception occurred while making a call to storeCustomer service", ioException);
        }
        return false;
    }

    private HttpPost createRequestPostMethod(final String requestUrl, final String requestJson, final ContentType contentType) {
        logger.info("Store Consumer Service URL : {}", requestUrl);
        HttpPost postMethod = null;
        try {
            postMethod = new HttpPost(requestUrl);
            final StringEntity stringEntity = new StringEntity(requestJson);
            postMethod.setHeader(new BasicHeader(HTTP.CONTENT_TYPE, contentType.toString()));
            postMethod.setEntity(stringEntity);
        } catch (final UnsupportedEncodingException unSupportedEncodingExcepton) {
            logger.error("An exception occurred while making a call to storeCustomer service", unSupportedEncodingExcepton);
        }
        return postMethod;
    }


}
