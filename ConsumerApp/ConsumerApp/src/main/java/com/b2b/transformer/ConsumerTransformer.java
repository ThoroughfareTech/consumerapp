package com.b2b.transformer;

import com.b2b.model.Consumer;

/**
 * Created by vkumar on 1/7/2017.
 */
public interface ConsumerTransformer {

    public String transform(Consumer consumer);
}
