package com.b2b.transformer;

import com.b2b.model.Consumer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vkumar on 1/7/2017.
 */
public class ConsumerRequestTransformer implements ConsumerTransformer {

    private static final Logger logger = LoggerFactory.getLogger(ConsumerRequestTransformer.class);

    @Override
    public String transform(final Consumer consumer) {
        final ObjectMapper objectMapper = new ObjectMapper();
        String consumerJson = "";
        try {
            consumerJson = objectMapper.writeValueAsString(consumer);
        } catch (final JsonProcessingException exception) {
            logger.error("An exception occurred ", exception);
        }

        return consumerJson;
    }
}
