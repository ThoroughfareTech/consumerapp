<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns:c="http://www.w3.org/1999/XSL/Transform">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        $('name').submit(function (event) {
            event.preventDefault();
            var $form = $(this), url = $form.attr('action');
            var posting = $.get(url, {username: $('#username').val(), password: $('#password').val()});
            posting.done(function (data) {
                console.log('success');
            });
        });
    </script>
</head>
<body onload='document.loginForm.username.focus();'>
<h3>Welcome to B2B website</h3>

<c:if test="${not empty error}">
    <div>${error}</div>
</c:if>
<c:if test="${not empty message}">
    <div>${message}</div>
</c:if>

<form name='login' action="authenticateLogin" method='get'>
    <table>
        <tr>
            <td>UserName:</td>
            <td><input type='text' name='username' value=''></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password'/></td>
        </tr>
        <tr>
            <td colspan='2'><input name="submit" id="login" type="submit" value="submit"/></td>
        </tr>
    </table>
</form>
</body>
</html>