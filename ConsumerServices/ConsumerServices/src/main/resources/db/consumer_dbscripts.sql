/*
Created: 11/27/2016
Modified: 12/5/2016
Model: PostgreSQL 9.5
Database: PostgreSQL 9.5
*/

-- Create schemas section -------------------------------------------------

CREATE SCHEMA "consumers"
;

CREATE SCHEMA "quotations"
;

-- Create tables section -------------------------------------------------

-- Table consumers.consumer

CREATE TABLE "consumers"."consumer"(
 "firstname" Character varying(100),
 "lastname" Character varying(100),
 "nickname" Character varying(100) NOT NULL,
 "email" Character varying(200) NOT NULL,
 "date_of_birth" Date,
 "status" Integer,
 "flags" Integer,
 "id" BigSerial NOT NULL,
 "time_created" Timestamp,
 "time_updated" Timestamp,
 "time_last_login" Timestamp,
 "primary_contact_id" Bigint,
 "mobile" Character varying(40) NOT NULL,
 "role" Integer
)
;
COMMENT ON COLUMN "consumers"."consumer"."status" IS 'Valid values are 0 -> YET_TO_SIGN_UP, 1 -> ACTIVE, 2 -> TEMP_LOCKED, 3 -> PROHIBITED, 4 -> CLOSED'
;
COMMENT ON COLUMN "consumers"."consumer"."primary_contact_id" IS 'Primary contact address of the customer, refers to customer_contact'
;
COMMENT ON COLUMN "consumers"."consumer"."mobile" IS 'Mobile number with country-code included'
;
COMMENT ON COLUMN "consumers"."consumer"."role" IS '0 - BUYER, 1 - DUAL'
;

-- Create indexes for table consumers.consumer

CREATE UNIQUE INDEX "CONSUMER_EMAIL_IDX" ON "consumers"."consumer" ("email")
;

CREATE UNIQUE INDEX "CONSUMER_MOBILE_IDX" ON "consumers"."consumer" ("mobile")
;

-- Add keys for table consumers.consumer

ALTER TABLE "consumers"."consumer" ADD CONSTRAINT "PK_consumer" PRIMARY KEY ("id")
;

ALTER TABLE "consumers"."consumer" ADD CONSTRAINT "t_user_email" UNIQUE ("email")
;

-- Table consumers.user_identtity_provider

CREATE TABLE "consumers"."user_identtity_provider"(
 "consumer_id" Bigint NOT NULL,
 "type" Integer DEFAULT 1 NOT NULL,
 "id" BigSerial NOT NULL
)
;
COMMENT ON COLUMN "consumers"."user_identtity_provider"."consumer_id" IS 'foreign key from User table'
;
COMMENT ON COLUMN "consumers"."user_identtity_provider"."type" IS 'Type identifier to qualify the credential. Valid values as of this writing are 1 -> user-secret, 2 -> google-id, 3 -> facebook-id'
;

-- Create indexes for table consumers.user_identtity_provider

CREATE INDEX "USER_ID_PROVIDER_CONSUMER_ID_IDX" ON "consumers"."user_identtity_provider" ("consumer_id")
;

-- Add keys for table consumers.user_identtity_provider

ALTER TABLE "consumers"."user_identtity_provider" ADD CONSTRAINT "PK_user_identtity_provider" PRIMARY KEY ("id")
;

-- Table consumers.user_external_credential

CREATE TABLE "consumers"."user_external_credential"(
 "consumer_id" Bigint NOT NULL,
 "external_user_id" Character varying(500) NOT NULL,
 "external_token" Character varying(500),
 "id" BigSerial NOT NULL,
 "time_token_expiry" Timestamp
)
;

-- Create indexes for table consumers.user_external_credential

CREATE INDEX "USER_EXT_CRED_CONSUMER_ID_IDX" ON "consumers"."user_external_credential" ("consumer_id")
;

-- Add keys for table consumers.user_external_credential
2
ALTER TABLE "consumers"."user_external_credential" ADD CONSTRAINT "PK_user_external_credential" PRIMARY KEY ("id")
;

-- Table consumers.user_internal_credential

CREATE TABLE "consumers"."user_internal_credential"(
 "id" BigSerial NOT NULL,
 "consumer_id" Bigint NOT NULL,
 "credential" Character varying(400) NOT NULL,
 "secret_question_answer_1" Character varying(1000) NOT NULL,
 "secret_question_answer_2" Character varying(1000) NOT NULL,
 "secret_question_answer_3" Character varying(1000) NOT NULL
)
;
COMMENT ON COLUMN "consumers"."user_internal_credential"."credential" IS 'Salted, encrypted password set by the consumer'
;
COMMENT ON COLUMN "consumers"."user_internal_credential"."secret_question_answer_1" IS 'Salted, encrypted password recovery data'
;
COMMENT ON COLUMN "consumers"."user_internal_credential"."secret_question_answer_2" IS 'Salted, encrypted password recovery data'
;
COzMMENT ON COLUMN "consumers"."user_internal_credential"."secret_question_answer_3" IS 'Salted, encrypted password recovery data'
;

-- Create indexes for table consumers.user_internal_credential

CREATE INDEX "USER_INT_CRED_CONSUMER_ID_IDX" ON "consumers"."user_internal_credential" ("consumer_id")
;

-- Add keys for table consumers.user_internal_credential

ALTER TABLE "consumers"."user_internal_credential" ADD CONSTRAINT "PK_user_internal_credential" PRIMARY KEY ("id")
;

ALTER TABLE "consumers"."user_internal_credential" ADD CONSTRAINT "consumerid" UNIQUE ("consumer_id")
;

-- Table quotations.quotation_request

CREATE TABLE "quotations"."quotation_request"(
 "id" BigSerial NOT NULL,
 "requestor_id" Bigint NOT NULL,
 "item_specification" Character varying(2000) NOT NULL,
 "time_created" Timestamp,
 "status" Integer,
 "time_updated" Timestamp,
 "item_name" Character varying(400) NOT NULL,
 "item_category" Character varying(1000),
 "quantity" Integer,
 "unit_of_measure" Character varying(20) NOT NULL,
 "expiry_time" Timestamp
)
;
COMMENT ON COLUMN "quotations"."quotation_request"."requestor_id" IS 'consumer_id of the buyer who requested quotation'
;
COMMENT ON COLUMN "quotations"."quotation_request"."item_specification" IS 'potentially JSON data for item specification'
;
COMMENT ON COLUMN "quotations"."quotation_request"."status" IS 'Valid values are 0 -> INITIATED, 1 -> APPROVED, 2 -> RUNNING, 3 -> CLOSED'
;
COMMENT ON COLUMN "quotations"."quotation_request"."unit_of_measure" IS 'Unit of measure like barrels, kg...'
;

-- Create indexes for table quotations.quotation_request

CREATE INDEX "QUOTATION_REQUEST_REQUESTOR_ID_IDX" ON "quotations"."quotation_request" ("requestor_id")
;

CREATE INDEX "QUOTATION_REQUEST_ITEM_CATAGORY_IDX" ON "quotations"."quotation_request" ("item_category")
;

-- Add keys for table quotations.quotation_request

ALTER TABLE "quotations"."quotation_request" ADD CONSTRAINT "PK_quotation_request" PRIMARY KEY ("id")
;

-- Table quotations.quotation_response

CREATE TABLE "quotations"."quotation_response"(
 "id" BigSerial NOT NULL,
 "quotation_id" Bigint NOT NULL,
 "merchant_id" Bigint NOT NULL,
 "response" Character varying(2000),
 "quoted_amount" Integer,
 "quoted_currency_code" Character varying(3)
)
;

-- Create indexes for table quotations.quotation_response

CREATE INDEX "QUOTATION_RESPONSE_QUOTATION_MERCHANT_ID_IDX" ON "quotations"."quotation_response" ("quotation_id","merchant_id")
;

-- Add keys for table quotations.quotation_response

ALTER TABLE "quotations"."quotation_response" ADD CONSTRAINT "PK_quotation_response" PRIMARY KEY ("id")
;

-- Table quotations.quotation_response_log

CREATE TABLE "quotations"."quotation_response_log"(
 "id" BigSerial NOT NULL,
 "quotation_id" Bigint NOT NULL,
 "response_id" Bigint NOT NULL,
 "quoted_amount" Bigint,
 "quoted_currency_code" Bigint,
 "response" Character varying(2000),
 "time_created" Timestamp NOT NULL
)
;

COMMENT ON TABLE "quotations"."quotation_response_log" IS 'This table can potentially move to a NOSQL setup for analytics. Has no real-time benifit to have this in RDBMS for now.'
;

-- Create indexes for table quotations.quotation_response_log

CREATE INDEX "QUOTATION_RESPONSE_LOG_QUOTATION_ID_IDX" ON "quotations"."quotation_response_log" ("quotation_id")
;

-- Add keys for table quotations.quotation_response_log

ALTER TABLE "quotations"."quotation_response_log" ADD CONSTRAINT "PK_quotation_response_log" PRIMARY KEY ("id")
;

-- Table consumers.consumer_contact

CREATE TABLE "consumers"."consumer_contact"(
 "address_line_1" Character varying(300) NOT NULL,
 "address_line_2" Character varying(300),
 "address_line_3" Character varying(300),
 "city" Character varying(100) NOT NULL,
 "state" Character varying(100) NOT NULL,
 "area_code" Character varying(20) NOT NULL,
 "country" Character varying(50) NOT NULL,
 "what3words_location" Character varying(300),
 "id" BigSerial NOT NULL,
 "consumer_id" Bigint NOT NULL
)
;

-- Create indexes for table consumers.consumer_contact

CREATE INDEX "CONSUMER_CONTACT_CONSUMER_ID_IDX" ON "consumers"."consumer_contact" ("consumer_id")
;

CREATE INDEX "CONSUMER_CONTACT_WHAT3WORDS_IDX" ON "consumers"."consumer_contact" ("what3words_location")
;

-- Add keys for table consumers.consumer_contact

ALTER TABLE "consumers"."consumer_contact" ADD CONSTRAINT "PK_consumer_contact" PRIMARY KEY ("id")
;

-- Table quotations.quotation_participant

CREATE TABLE "quotations"."quotation_participant"(
 "id" BigSerial NOT NULL,
 "quotation_id" Bigint NOT NULL,
 "merchant_id" Bigint NOT NULL,
 "status" Integer
)
;
COMMENT ON COLUMN "quotations"."quotation_participant"."status" IS 'Says what is the status of the merchant participation. Valid values are 0 -> INVITED, 1 -> ACCEPTED, 2 -> DECLINED, 3 -> NOT_REACHABLE, 4 -> CLOSED'
;

-- Create indexes for table quotations.quotation_participant

CREATE UNIQUE INDEX "QUOTATION_PARTICIPATION_IDX" ON "quotations"."quotation_participant" ("merchant_id","quotation_id")
;

-- Add keys for table quotations.quotation_participant

ALTER TABLE "quotations"."quotation_participant" ADD CONSTRAINT "PK_quotation_participant" PRIMARY KEY ("id")
;

-- Table consumers.merchant_business_profile

CREATE TABLE "consumers"."merchant_business_profile"(
 "id" BigSerial NOT NULL,
 "business_type" Character varying(100) NOT NULL,
 "tax_identification_number" Character varying(100),
 "consumer_id" Bigint NOT NULL
)
;
COMMENT ON COLUMN "consumers"."merchant_business_profile"."business_type" IS ';'''
;

-- Create indexes for table consumers.merchant_business_profile

CREATE INDEX "MERCHANT_BUSINESS_PROFILE_CONSUMER_ID_IDX" ON "consumers"."merchant_business_profile" ("consumer_id")
;

CREATE INDEX "MERCHANT_BUSINESS_PROFILE_TYPE_IDX" ON "consumers"."merchant_business_profile" ("business_type")
;

-- Add keys for table consumers.merchant_business_profile

ALTER TABLE "consumers"."merchant_business_profile" ADD CONSTRAINT "PK_merchant_business_profile" PRIMARY KEY ("id")
;




