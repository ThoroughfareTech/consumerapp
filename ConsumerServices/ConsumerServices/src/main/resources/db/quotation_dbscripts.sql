
CREATE SCHEMA quotations;


SET search_path = consumers, pg_catalog, quotations;

SET default_tablespace = '';

SET default_with_oids = false;


--
-- Name: quotation_request; Type: TABLE; Schema: quotations; Owner: -
--

CREATE TABLE quotation_request (
    id bigint NOT NULL,
    requestor_id bigint NOT NULL,
	group_id bigint NOT NULL,
    item_specification character varying(2000) NOT NULL,
    time_created timestamp without time zone,
    status integer,
    time_updated timestamp without time zone,
    expiry_time timestamp without time zone,
	accepted_supplier_id bigint
);

CREATE SEQUENCE quotation_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
--
-- Name: COLUMN quotation_request.requestor_id; Type: COMMENT; Schema: quotations; Owner: -
--

COMMENT ON COLUMN quotation_request.requestor_id IS 'consumer_id of the buyer who requested quotation';


--
-- Name: COLUMN quotation_request.item_specification; Type: COMMENT; Schema: quotations; Owner: -
--

COMMENT ON COLUMN quotation_request.item_specification IS 'potentially JSON data for item specification';


--
-- Name: COLUMN quotation_request.status; Type: COMMENT; Schema: quotations; Owner: -
--

COMMENT ON COLUMN quotation_request.status IS 'Valid values are 0 -> INITIATED, 1 -> APPROVED, 2 -> RUNNING, 3 -> CLOSED';


--
-- Name: quotation_request_id_seq; Type: SEQUENCE; Schema: quotations; Owner: -
--

CREATE SEQUENCE quotation_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY quotation_request ALTER COLUMN id SET DEFAULT nextval('quotation_request_id_seq'::regclass);

--
-- Name: quotation_request_id_seq; Type: SEQUENCE OWNED BY; Schema: quotations; Owner: -
--

ALTER SEQUENCE quotation_request_id_seq OWNED BY quotation_request.id;

CREATE TABLE quotation_request_item (
    id bigint NOT NULL,
    quotation_request_id bigint NOT NULL,
    item_name character varying(400) NOT NULL,
    item_category character varying(1000),
    quantity integer,
    unit_of_measure character varying(20) NOT NULL
);

CREATE SEQUENCE quotation_request_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE quotation_request_item_id_seq OWNED BY quotation_request_item.id;

ALTER TABLE ONLY quotation_request_item ALTER COLUMN id SET DEFAULT nextval('quotation_request_item_id_seq'::regclass);

--
-- Name: COLUMN quotation_request.unit_of_measure; Type: COMMENT; Schema: quotations; Owner: -
--

COMMENT ON COLUMN quotation_request_item.unit_of_measure IS 'Unit of measure like barrels, kg...';

	
--
-- Name: quotation_response; Type: TABLE; Schema: quotations; Owner: -
--

CREATE TABLE quotation_response (
    id bigint NOT NULL,
    quotation_id bigint NOT NULL,
    merchant_id bigint NOT NULL,
    response character varying(2000),
    quoted_amount integer,
    quoted_currency_code character varying(3)
);


--
-- Name: quotation_response_id_seq; Type: SEQUENCE; Schema: quotations; Owner: -
--

CREATE SEQUENCE quotation_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: quotation_response_id_seq; Type: SEQUENCE OWNED BY; Schema: quotations; Owner: -
--

ALTER SEQUENCE quotation_response_id_seq OWNED BY quotation_response.id;
