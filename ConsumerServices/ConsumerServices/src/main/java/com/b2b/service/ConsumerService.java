package com.b2b.service;

import com.b2b.dao.ConsumerDAO;
import com.b2b.model.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

/**
 * Service class for Consumer.
 *
 * @author vkumar 5/1/2017
 */

@Service
public class ConsumerService {

    private final ConsumerDAO consumerDAO;

    @Autowired
    public ConsumerService(final ConsumerDAO consumerDAO) {
        this.consumerDAO = consumerDAO;
    }

    public long save(final Consumer consumer) throws ParseException {
        return consumerDAO.create(consumer);
    }

    public List<Consumer> checkExistingUser(final String email, final String mobile) {
        return consumerDAO.checkExistingUser(email, mobile);
    }

    public Consumer getConsumer(final long id) {
        return consumerDAO.getConsumer(id);
    }
}
