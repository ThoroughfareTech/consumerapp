package com.b2b.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2b.dao.QuotationDAO;
import com.b2b.model.Quotation;

@Service
public class QuotationService {

	@Autowired
	private QuotationDAO quotationDAO;
	
	public Long save(Quotation quotation) {
		return quotationDAO.create(quotation);
	}
}
