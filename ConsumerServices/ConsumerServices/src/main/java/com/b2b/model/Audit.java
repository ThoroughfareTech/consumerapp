package com.b2b.model;

import java.util.Date;

public class Audit {
	
	private Date createdDate;
	private Date updatedDate;
	
	public Audit() {
		this.createdDate = new Date();
		this.updatedDate = new Date();
	}

	public Date getCreatedDate() {
		return createdDate;
	}	
	
	public Date getUpdatedDate() {
		return updatedDate;
	}
}
