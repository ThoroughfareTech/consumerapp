package com.b2b.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserCredential {

    private final String credential;

    public UserCredential(@JsonProperty("credential") String credential) {
        this.credential = credential;
    }

    public String getCredential() {
        return credential;
    }

}
