package com.b2b.model;

import java.util.List;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Quotation implements Auditable{

	public enum Status{
		INITIATED, APPROVED, RUNNING, CLOSED;
	}
	
	private Long id;	
	private Status status;
	private String sellerNotes;
	@JsonIgnore
	private Audit audit;
	private List<Item> items;
	@JsonIgnore
	private Buyer buyer;
	@JsonIgnore
	private Group group;
	
	@JsonIgnore
	private Type type;
	/*public Quotation() {
		
	}*/
	
	public Quotation(
			 @JsonProperty(value = "id") final Long id,
			 @JsonProperty(value = "status") final Status status,
			 @JsonProperty("sellerNotes") final String sellerNotes,
			 @JsonProperty("items") final List<Item> items,
			 @JsonProperty("buyerId") final Long buyerId,
			 @JsonProperty(value = "groupId") final Long groupId) {
		this.buyer = new Buyer(buyerId);
		this.group = new Group(groupId);
		this.audit = new Audit();
		this.items = items;
		this.sellerNotes = sellerNotes;
		this.id = id;
		this.status = status;
	}
	
	/*public Quotation(
			 @JsonProperty(value = "id") final Long id,
			 @JsonProperty(value = "status") final Status status,
			 @JsonProperty(value = "sellerNotes") final String sellerNotes,
			 @JsonProperty(value = "items") final List<Item> items,
			 @JsonProperty(value = "buyerId") final Long buyerId,
			 @JsonProperty(value = "groupId") final Long groupId,
			 Type type) {
		this(id, status, sellerNotes, items, buyerId, groupId);
		this.type = type;		
	}*/
	
	public static class Item {
		private String name;
		private Long quantity;
		private String unit;
		
		@Override
		public String toString() {
			StringJoiner joiner = new StringJoiner(",");
			return joiner.add(name).add(String.valueOf(quantity)).add(unit).toString();
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Long getQuantity() {
			return quantity;
		}

		public void setQuantity(Long quantity) {
			this.quantity = quantity;
		}

		public String getUnit() {
			return unit;
		}

		public void setUnit(String unit) {
			this.unit = unit;
		}
	}
	
	public static class Request{
		
	}
	
	public static class Response{
		
	}
	
	public static class Group{
		private Long groupId;
		public Group(Long groupId) {
			this.groupId = groupId;
		}
		public Long getGroupId() {
			return groupId;
		}
	}
	
	public static class Buyer{
		private Long buyerId;
		public Buyer(Long buyerId) {
			this.buyerId = buyerId;
		}
		
		@Override
		public String toString() {
			StringJoiner joiner = new StringJoiner(",");
			return joiner.add(Long.toString(buyerId)).toString();
		}

		public Long getBuyerId() {
			return buyerId;
		}

	}

	@Override
	@JsonIgnore
	public Audit getAudit() {
		return this.audit;
	}
	
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	
	@JsonIgnore
	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(",");
		joiner.add("Items "+items+"");
		joiner.add("Notes:" + sellerNotes);
		joiner.add("ReqUserid:" + buyer);
		return joiner.toString();
	}
	public String getSellerNotes() {
		return sellerNotes;
	}
	public void setSellerNotes(String sellerNotes) {
		this.sellerNotes = sellerNotes;
	}
	
	public Quotation createQuotation() {
		Quotation quotation = new Quotation(null, Status.INITIATED, sellerNotes, items, this.buyer.buyerId, this.group.groupId);
		return quotation;
	}
	
	public Quotation copy(Long id) {
		Quotation quotation = new Quotation(id, this.status, sellerNotes, items, this.buyer.buyerId, this.group.groupId);
		return quotation;
	}

	public Long getId() {
		return id;
	}

	public Status getStatus() {
		return status;
	}

	public Buyer getBuyer() {
		return buyer;
	}

	public Group getGroup() {
		return group;
	}

	public Type getType() {
		return type;
	}
}
