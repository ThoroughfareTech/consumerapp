package com.b2b.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Auditable {
	
	Audit getAudit();
	
	@JsonIgnore
	default Date getCreatedDate() {
		return getAudit().getCreatedDate();
	}
	
	@JsonIgnore
	default Date getUpdatedDate() {
		return getAudit().getUpdatedDate();
	}
}
