package com.b2b.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The bean class for Consumer
 * Created by vkumar on 1/7/2017.
 */
public class Consumer {

    private final Long id;

    private final String firstName;

    private final String lastName;

    private final String nickName;

    private final String companyName;

    private final String email;

    private final String dateOfBirth;

    private final int status;

    private final int flags;

    private final String mobile;

    private final int role;

    private final UserCredential credential;

    public Consumer(
            @JsonProperty(value = "id") final Long id,
            @JsonProperty("mobile") final String mobile,
            @JsonProperty("FirstName") final String firstName,
            @JsonProperty("LastName") final String lastName,
            @JsonProperty("NickName") final String nickName,
            @JsonProperty("CompanyName") final String companyName,
            @JsonProperty("Email") final String email,
            @JsonProperty("dob") final String dob,
            @JsonProperty("status") final int status,
            @JsonProperty("role") final int role,
            @JsonProperty("flags") final int flags, @JsonProperty("credential") final UserCredential credential) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.companyName = companyName;
        this.email = email;
        this.dateOfBirth = dob;
        this.status = status;
        this.flags = flags;
        this.mobile = mobile;
        this.role = role;
        this.credential = credential;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getEmail() {
        return email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public int getStatus() {
        return status;
    }

    public int getFlags() {
        return flags;
    }

    public String getMobile() {
        return mobile;
    }

    public int getRole() {
        return role;
    }

    public Long getId() {
        return id;
    }

    public UserCredential getCredential() {
        return credential;
    }

    public Consumer copyWithId(long id) {
        return new Consumer(id, this.mobile, this.firstName, this.lastName, this.nickName, this.companyName, this.email, this.dateOfBirth, this.status, this.role, this.flags, this.credential);
    }

}
