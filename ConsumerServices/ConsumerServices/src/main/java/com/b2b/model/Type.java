package com.b2b.model;

public enum Type {
	CREATE, MODIFY, DELETE, QUERY
}
