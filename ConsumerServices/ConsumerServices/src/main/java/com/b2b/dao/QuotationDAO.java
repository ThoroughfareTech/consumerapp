package com.b2b.dao;

import java.text.SimpleDateFormat;

import com.b2b.model.Quotation;

public interface QuotationDAO {
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
	
	Long create(Quotation quotation);
}
