package com.b2b.dao;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.b2b.model.Quotation;

public class JDBCQuotationDAO implements QuotationDAO{

	@Autowired
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    private static final String CREATE_QUOTATION_REQUEST =
            "INSERT INTO quotations.quotation_request (requestor_id, group_id, item_specification, "
                    + "time_created, status, expiry_time) VALUES \n"
                    + "(:requestorId, :groupId, :sellerNotes, :timeCreated, "
                    + ":status, :expiryTime)";
    
    private static final String CREATE_QUOTATION_REQ_ITEM =
            "INSERT INTO quotations.quotation_request_item (quotation_request_id, item_name, item_category, "
                    + "quantity, unit_of_measure) VALUES \n"
                    + "(:quotationId, :itemName, :itemCategory, :quantity, "
                    + ":unitOfMeasure)";
        
	@Override
	@Transactional
	public Long create(Quotation quotation) {
		
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		LocalDate today = LocalDate.now();
		Instant instantNow = today.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		parameterJdbcTemplate.update(
				CREATE_QUOTATION_REQUEST,
                new MapSqlParameterSource()
                        .addValue("requestorId", quotation.getBuyer().getBuyerId())
                        .addValue("groupId", quotation.getGroup().getGroupId())
                        .addValue("sellerNotes", quotation.getSellerNotes())
                        .addValue("status", quotation.getStatus().ordinal())                        
                        .addValue("timeCreated", Date.from(instantNow))
                        .addValue("expiryTime", Date.from(today.plusDays(5).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())),                        
                keyHolder,
                new String[]{"id"}
        );

        final long quotationId = keyHolder.getKey().longValue();
        
        System.out.println("Quoation id:"+quotationId);
        quotation.getItems().stream().forEachOrdered( item ->
        	parameterJdbcTemplate.update(
        		CREATE_QUOTATION_REQ_ITEM,
                new MapSqlParameterSource()
                        .addValue("quotationId", quotationId)
                        .addValue("itemName", item.getName())
                        .addValue("itemCategory", "")
                        .addValue("quantity", item.getQuantity())
                        .addValue("unitOfMeasure", item.getUnit()),
                keyHolder,
                new String[]{"id"}));

        return quotationId;
	}

}
