package com.b2b.dao;

import com.b2b.model.Consumer;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * JDBC class for Consumer Bean.
 *
 * @author vkumar 5/1/2017
 */
@SuppressWarnings("HardcodedLineSeparator")
public class JDBCConsumerDAO implements ConsumerDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(JDBCConsumerDAO.class);

    private static final String CREATE_EMPLOYEE_INFO =
            "INSERT INTO consumers.consumer (firstname, lastname, nickname, company_name, email, date_of_birth, status, \n"
                    + "            flags, time_created, time_updated, time_last_login, primary_contact_id, \n"
                    + "            mobile, role) VALUES "
                    + "(:firstName, :lastName, :nickName, :company_name, :email, :dob,:status,:flags, :time_created, :time_updated, "
                    + ":time_last_login, :primary_contact_id, :mobile, :role)";

    @Autowired
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    private static final String CREATE_INTERNAL_USER_CREDENTIAL =
            "INSERT INTO consumers.user_internal_credential (consumer_id, credential, secret_question_answer_1, "
                    + "secret_question_answer_2, secret_question_answer_3) VALUES \n"
                    + "(:consumer_id, :credential, :secret_question_answer_1, :secret_question_answer_2, "
                    + ":secret_question_answer_3)";

    private static final String GET_CREDENTIAL_FOR_CONSUMER =
            "select credential from consumers.user_internal_credential where consumer_id=:consumer_id";

    private static final String SELECT_USER_BY_MOBILE_OR_EMAIL =
            "select * from consumers.consumer where mobile=:mobile or email=:email ";

    private static final String SELECT_USER_BY_MOBILE =
            "select * from consumers.consumer where mobile=:mobile ";

    private static final String SELECT_USER_BY_EMAIL =
            "select * from consumers.consumer where email=:email ";

    private static final String SELECT_USER_BY_ID =
            "select * from consumers.consumer where id=:id";

    private final RowMapper<Consumer> rowMapper = new ConsumerRowMapper();

    /**
     * This method inserts the consumer and returns consumer id.
     *
     * @param consumer
     * @return int (Consumer ID)
     * @throws ParseException
     */
    @Override
    public long create(final Consumer consumer) throws ParseException {
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        @SuppressWarnings("SimpleDateFormatWithoutLocale")
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");

        parameterJdbcTemplate.update(
                CREATE_EMPLOYEE_INFO,
                new MapSqlParameterSource()
                        .addValue(JDBCConstants.FIRST_NAME, consumer.getFirstName())
                        .addValue(JDBCConstants.LAST_NAME, consumer.getLastName())
                        .addValue(JDBCConstants.NICK_NAME, consumer.getNickName())
                        .addValue(JDBCConstants.COMPANY_NAME, consumer.getCompanyName())
                        .addValue(JDBCConstants.EMAIL, consumer.getEmail())
                        .addValue(JDBCConstants.DOB, consumer.getDateOfBirth() == null ? null : simpleDateFormat.parse(consumer.getDateOfBirth()))
                        .addValue(JDBCConstants.STATUS, consumer.getStatus())
                        .addValue(JDBCConstants.FLAGS, consumer.getFlags())
                        .addValue(JDBCConstants.TIME_CREATED, new Date())
                        .addValue(JDBCConstants.TIME_UPDATED, new Date())
                        .addValue(JDBCConstants.TIME_LAST_LOGIN, new Date())
                        .addValue(JDBCConstants.PRIMARY_CONCAT_ID, 123)
                        .addValue(JDBCConstants.MOBILE, consumer.getMobile())
                        .addValue(JDBCConstants.ROLE, consumer.getRole()),
                keyHolder,
                new String[]{"id"}
        );

        final long consumerId = keyHolder.getKey().longValue();

        parameterJdbcTemplate.update(
                CREATE_INTERNAL_USER_CREDENTIAL,
                new MapSqlParameterSource()
                        .addValue(JDBCConstants.CREDENTIAL, consumer.getCredential().getCredential())
                        .addValue(JDBCConstants.CONSUMER_ID, consumerId)
                        .addValue(JDBCConstants.SECRET_QUESTION_ANSWER_ID1, "")
                        .addValue(JDBCConstants.SECRET_QUESTION_ANSWER_ID2, "")
                        .addValue(JDBCConstants.SECRET_QUESTION_ANSWER_ID3, ""),
                keyHolder,
                new String[]{"id"});

        return consumerId;

    }

    /**
     * It checks for the existing consumer based on
     *
     * @param email
     * @param mobile
     * @return Consumer lst.
     */
    @Override
    public List<Consumer> checkExistingUser(final String email, final String mobile) {
        try {
            if (email != null && mobile != null) {
                return parameterJdbcTemplate.query(
                        SELECT_USER_BY_MOBILE_OR_EMAIL,
                        ImmutableMap.of(JDBCConstants.MOBILE, mobile, JDBCConstants.EMAIL, email),
                        rowMapper);
            } else if (email != null) {
                return parameterJdbcTemplate.query(
                        SELECT_USER_BY_EMAIL,
                        ImmutableMap.of(JDBCConstants.EMAIL, email),
                        rowMapper);

            } else if (mobile != null) {
                return parameterJdbcTemplate.query(
                        SELECT_USER_BY_MOBILE,
                        ImmutableMap.of(JDBCConstants.MOBILE, mobile),
                        rowMapper);
            } else {
                return null;
            }
        } catch (final EmptyResultDataAccessException emptyResultDataAccessException) {
            LOGGER.error("No  existing user found!");
            return null;
        }
    }

    /**
     * This method fetches the consumer based on consumer id.
     *
     * @param id
     * @return Consumer
     */
    @Override
    public Consumer getConsumer(final long id) {
        try {
            final Consumer consumer = parameterJdbcTemplate.queryForObject(
                    SELECT_USER_BY_ID,
                    ImmutableMap.of(JDBCConstants.ID, id),
                    rowMapper);

            return consumer;
        } catch (final EmptyResultDataAccessException emptyResultDataAccessException) {
            LOGGER.error("No  existing user found");
            return null;
        }
    }

    @Override
    public String getConsumerCredential(long id) {
        return parameterJdbcTemplate.queryForObject(GET_CREDENTIAL_FOR_CONSUMER, ImmutableMap.of(JDBCConstants.CONSUMER_ID, id), String.class);
    }
}
