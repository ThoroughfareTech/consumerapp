package com.b2b.dao;

import com.b2b.model.Consumer;

import java.text.ParseException;
import java.util.List;

/**
 * @author vkumar 5/1/2017
 */
public interface ConsumerDAO {

    long create(Consumer consumer) throws ParseException;

    List<Consumer> checkExistingUser(final String email, final String mobile);

    Consumer getConsumer(final long id);

    String getConsumerCredential(final long id);

}
