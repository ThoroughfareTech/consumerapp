package com.b2b.dao;

import com.b2b.model.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author vkumar 5/19/2017
 */
public class ConsumerRowMapper implements RowMapper<Consumer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerRowMapper.class);

    @Override
    public Consumer mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        return new Consumer(
                rs.getLong("id"),
            rs.getString("mobile"),
            rs.getString("firstname"),
            rs.getString("lastname"),
            rs.getString("nickname"),
                rs.getString("company_name"),
            rs.getString("email"),
                rs.getDate("date_of_birth") == null ? null : rs.getDate("date_of_birth").toString(),
            rs.getInt("status"),
            rs.getInt("role"),
            rs.getInt("flags"),
                null);
    }
}
