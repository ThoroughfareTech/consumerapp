package com.b2b.controller;

import com.b2b.model.Consumer;
import com.b2b.service.ConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author vkumar 1/7/2017.
 */
@RestController
public class ConsumerServiceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerServiceController.class);

    @Autowired
    private final ConsumerService consumerService;

    @Autowired
    public ConsumerServiceController(
        final ConsumerService consumerService) {
        this.consumerService = consumerService;
    }

    @RequestMapping(
            value = "/v1/users/",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<Object> createUser(
            @RequestBody final Consumer consumer) throws ParseException {
        LOGGER.debug("Got the user creation request : {}", consumer.toString());

        final Optional<List<Consumer>> existingConsumer =
            Optional.ofNullable(consumerService.checkExistingUser(consumer.getEmail(), consumer.getMobile()));

        if (existingConsumer.isPresent() && !existingConsumer.get().isEmpty()) {
            return ResponseEntity.status(HttpStatus.FOUND).body(existingConsumer.get());
        }
        final long userIdentifier = consumerService.save(consumer);
        LOGGER.info("Created user with identifier:" + userIdentifier);
        Consumer updatedConsumer = consumer.copyWithId(userIdentifier);
        return ResponseEntity.status(HttpStatus.CREATED).body(updatedConsumer);
    }

    @RequestMapping(
            value = "/v1/users/{user-id}", produces = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.GET)
    @CrossOrigin
    @ResponseBody
    public ResponseEntity<Consumer> getConsumerInfo(@PathVariable("user-id") final long id) {
        LOGGER.info("Get the consumer from service!!! {}", id);
        final Optional<Consumer> consumer = Optional.ofNullable(consumerService.getConsumer(id));
        if (consumer.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(consumer.get());
        }
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(
            value = "/v1/users", produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET)
    @CrossOrigin
    @ResponseBody
    public ResponseEntity<List<Consumer>> searchConsumers(@RequestParam("email") final String email, @RequestParam("mobile") final String mobile) {
        LOGGER.info("Search {},{}", mobile, email);
        final Optional<List<Consumer>> consumers = Optional.ofNullable(consumerService.checkExistingUser(email, mobile));
        if (consumers.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(consumers.get());
        }
        return ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList());
    }

}