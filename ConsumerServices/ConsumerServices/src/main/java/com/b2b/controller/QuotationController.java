package com.b2b.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2b.model.Quotation;
import com.b2b.service.QuotationService;

@RestController
public class QuotationController {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(QuotationController.class);
	 
	 @Autowired
	 private QuotationService quotationService;
	 
	 @RequestMapping(value = "/v1/quotations",
	        consumes = MediaType.APPLICATION_JSON_VALUE,
	        method = RequestMethod.POST)
	 @CrossOrigin
	 public ResponseEntity<Object> newQuotation(@RequestBody final Quotation quotation) throws ParseException {
        LOGGER.debug("Got the new quotation : {}", quotation);
        Quotation newQuotation = quotation.createQuotation();
        Long id = quotationService.save(newQuotation);
        LOGGER.debug("Created quotation : {}", id);
        return ResponseEntity.status(HttpStatus.CREATED).body(newQuotation.copy(id));
	 }

	 
}
