package com.b2b;

import com.b2b.config.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.b2b, com.tft.services")
@Import({SwaggerConfig.class})
public class ConsumerServicesApplication {

    public static void main(final String[] args) {
        SpringApplication.run(ConsumerServicesApplication.class, args);
    }
}
