package com.tft.services.identity

import com.b2b.dao.ConsumerDAO
import com.b2b.model.Consumer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import java.util.*

data class User(val email: String?, val mobile: String?)
data class LoginRequest(val user: User, val credential: String)
enum class CredentialStatus {VALID, INVALID }

sealed class ApplicationException(override val message: String) : Throwable(message = message)
class UserNotFoundException : ApplicationException(message = "User email/mobile or password is not valid")
class UserNotValidException : ApplicationException(message = "User email/mobile or password is not valid")
class UnexpectedStateException : ApplicationException(message = "Some system error occured. Please try again later")

@RestController
@RequestMapping("/v1/identity")
class LoginController(val consumerDAO: ConsumerDAO) {

    val logger: Logger = LoggerFactory.getLogger(LoginController::class.java)

    @PostMapping("login")
    @CrossOrigin
    fun login(@RequestBody loginRequest: LoginRequest): Consumer {
        logger.info("Login request received for {}", loginRequest)
        val consumers: List<Consumer> = loginRequest.user.email?.let { consumerDAO.checkExistingUser(it, null) } ?: loginRequest.user.mobile?.let { consumerDAO.checkExistingUser(null, it) } ?: Collections.emptyList()
        when (consumers.size) {
            1 -> when (validateCredentials(loginRequest.credential, consumers[0])) {
                CredentialStatus.VALID -> return consumers[0]
                CredentialStatus.INVALID -> throw UserNotValidException()
            }
            0 -> throw UserNotFoundException()
            else -> throw UnexpectedStateException()
        }
    }

    private fun validateCredentials(credential: String, consumer: Consumer): CredentialStatus {
        val credentialFromDB: String? = consumerDAO.getConsumerCredential(consumer.id)
        return credentialFromDB?.let { if (it.equals(credential)) CredentialStatus.VALID else CredentialStatus.INVALID } ?: CredentialStatus.INVALID
    }

}