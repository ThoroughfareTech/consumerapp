package com.tft.services.identity

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.lang.RuntimeException

@ControllerAdvice
class RestExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = ApplicationException::class)
    fun handleApplicationException(exception: ApplicationException, request: WebRequest): ResponseEntity<Any> = when (exception) {
        is UserNotFoundException, is UserNotValidException -> handleExceptionInternal(RuntimeException(), exception.message, HttpHeaders(), HttpStatus.FORBIDDEN, request)
        is UnexpectedStateException -> handleExceptionInternal(RuntimeException(), exception.message, HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request)
    }
}